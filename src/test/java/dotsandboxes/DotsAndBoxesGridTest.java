package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }


    @Test
    public void callingboxCompleteOnACompleteBoxShouldReturnTrue() {
	logger.info("Testing boxComplete() on a complete box");
	var grid = new DotsAndBoxesGrid(2, 2, 1); // Minimum sized grid to test (1 box)
	grid.drawHorizontal(0, 0, 1);
	grid.drawHorizontal(0, 1, 1);
	grid.drawVertical(0, 0, 1);
	grid.drawVertical(1, 0, 1);

	assertTrue(grid.boxComplete(0, 0));
    }

    @Test
    public void callingboxCompleteOnAnIncompleteBoxShouldReturnFalse() {
	logger.info("Testing boxCopmlete() on an incomplete box");

	var grid = new DotsAndBoxesGrid(2, 2, 1);

	// Build a box testing after each line
	// All checks should return false except the last which should be a complete box
	assertFalse(grid.boxComplete(0, 0));
	grid.drawHorizontal(0, 0, 0);
	assertFalse(grid.boxComplete(0, 0));
	grid.drawHorizontal(0, 1, 0);
	assertFalse(grid.boxComplete(0, 0));
	grid.drawVertical(0, 0, 0);
	assertFalse(grid.boxComplete(0, 0));
	grid.drawVertical(1, 0, 0);
	assertTrue(grid.boxComplete(0, 0));

    }

    @Test
    public void drawingAHorizontalLineOverAnAlreadyDrawnLineShouldThrow() {
	logger.info("Testing horizontal line redraws.");

	var grid = new DotsAndBoxesGrid(2, 2, 1);

	grid.drawHorizontal(0, 0, 1);
	assertThrows(IllegalStateException.class, () -> grid.drawHorizontal(0, 0, 1));
    }
}
